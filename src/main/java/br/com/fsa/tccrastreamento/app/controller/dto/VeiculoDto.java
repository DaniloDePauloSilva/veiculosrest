package br.com.fsa.tccrastreamento.app.controller.dto;

import java.time.LocalDateTime;

import br.com.fsa.tccrastreamento.app.model.Veiculo;

public class VeiculoDto {
	
	private Long id;
	private String placa;
	private String descricao;
	private String marca;
	private String modelo;
	private Integer ano;
	private String cor;
	private LocalDateTime dataCadastro;
	
	public VeiculoDto(Veiculo veiculo) 
	{
		this.id = veiculo.getId(); 
		this.placa = veiculo.getPlaca();
		this.descricao = veiculo.getDescricao();
		this.dataCadastro = veiculo.getDataCadastro();
		this.setMarca(veiculo.getMarca());
		this.setModelo(veiculo.getModelo());
		this.setAno(veiculo.getAno());
		this.setCor(veiculo.getCor());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
	

}
